Datová nalýza pomocí knihovny Pandas
=====================================

- **Lektor:** Ing. Jan kolomazník, Ph.D.
- **Mobil:** +420 732 568 669
- **Email:** jan.kolomaznik@gmail.com 

Python je jednoduchý a zároveň mocný programovací jazyk. 
Přestože se velmi lehce se učí, využívá se v různých oblastech - od zpracování vědeckých dat, přes zpracování obrazu až k výrobě webových stránek.
Pomocí Pythonu můžete zautomatizovat pracné části své práce a tím si zjednodušit život.
V rámci školení se seznámíte s fungováním jazyka Python, podíváte se na složitější příkazy a možnostmi hledání a využití standardní knihovny. 
Součástí kurzu jsou samozřejmě také praktická cvičení, během nichž mimo jiné vyzkoušíte vytvoření automatizace dvou úkolů. 

Náplň kurzu:
------------

- **Stručný úvod do Pythonu**
    - **[Seznámení s jazykem Python](00-python.ipynb)**
    - **[Základy jazyka](01-python.basic.ipynb)**
    - **[Datové typy](02-python.type.ipynb)**
    - **[Základní datové struktury](03-python.structure.ipynb)**
    - **[Základní konstrukce jazyka](04-python.construction.ipynb)**
    - **[Funkce](05-python.function.ipynb)**
- **[Úvod do datové analýzy (data sience)](06-pandas.ipynb)**
    - Proces analýzy dat
    - Pandas
    - Příprava dat
    - Ověření hypotézy
    * Proces datové analýzy
    * Formulace problému
    * Strojové učení vs. Statistické metody
    * Přehled nejpoužívanějších knihoven
- **[Zdákladní datové typy](07-pandas.types.ipynb)**
    - Posloupnost (Series)
    - Tabulky (DataFrame)
    - Vytvoření tabulky
- **[Sloupce (Series)](08-pandas.series.ipynb)**
    - Operace
    - Intexy, řezy a masky
    - Spojování serii
    - Další metody
    - Další užitečné funkce
    * Načítání z různých zdrojů (CSV, Excel, HTML)
    * Transformace a úpravy a filtrování
    * Ukládání a prezentace
- **[Tabulky (DataFrame)](09-pandas.dataframe.ipynb)**
    - Výběr prvků
    - Osy DataFrame
    - Mazání z DaraFrame
    - Přejmenování
    - Indexer `loc`
    - Indexer `iloc`
    - Indexy
    - NaN neboli NULL či N/A
    - Merge
    - Groupby
    - Pivot table
    * Načítání z různých zdrojů (CSV, Excel, HTML)
    * Transformace a úpravy a filtrování
    * Ukládání a prezentace
- **[Grafy](10-pandas.plot.ipynb)**
    - Přehled základních grafů:
    * Různé druhy grafů 
    
Bonusy
------

- **[Numerical Python (NumPy)](11-numpy.ipynb)**
    - Vytváření polí
    - Operace
    - Vybrané statistické funkce
    - Indexy a řezy
    - Kopírování dat
- **[Lineární regrese](12-Linear_Regression.ipynb)**
    - Nastavení prostředí
    - Vstupní Data
    - Zpracová dat
    - Lineární regrese
    - Lineární regrese 2
    - Použití modelu
- **[Itertools](13-python.function.itertools.ipynb)**
    - Co je to Itertools
    - Proč Itertools používat?
    - Funkce `itertools.zip_longest()`
    - Et tu, Brute Force?
    - Sekvence čísel
    - Rekurze
    - Karetní příklad
    - Odbočení: Práce se seznam seznamů
    - Filtrování
    - Seskupení
